﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [Range(0, 1)]
    [SerializeField] private float m_musicVolume = 1.0f, m_fxVolume = 1.0f;
    public float musicVolume { set { m_musicVolume = value; } get { return m_musicVolume; } }
    public float fxVolume { set { m_fxVolume = value; } get { return m_fxVolume; } }

    [SerializeField]private AudioClip m_clearSound, m_moveSound, m_gameOverSound, m_backGroundMusic, m_winSound, m_dropSound, m_errorSound;
    public AudioClip clearSound { set { m_clearSound = value; } get { return m_clearSound; } }
    public AudioClip moveSound { set { m_moveSound = value; } get { return m_moveSound; } }
    public AudioClip gameOverSound { set { m_gameOverSound = value; } get { return m_gameOverSound; } }
    public AudioClip winSound { set { m_winSound = value; } get { return m_winSound; } }
    public AudioClip dropSound { set { m_dropSound = value; } get { return m_dropSound; } }
    public AudioClip errorSound { set { m_errorSound = value; } get { return m_errorSound; } }

    [SerializeField] private AudioSource m_musicSource;

    private void Start ()
    {
        PlayBackGroundMusic(m_backGroundMusic);
	}
	
	public void PlayBackGroundMusic(AudioClip musicClip)
    {
        if(m_musicSource && musicClip)
        {
            m_musicSource.Stop();
            m_musicSource.clip = musicClip;
            m_musicSource.volume = m_musicVolume;
            m_musicSource.loop = true;
            m_musicSource.Play();
        }
    }

}
