﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : GamePiece {

    private ScoreManager scoreManager;

    private void Awake()
    {
        scoreManager = GamePlayManager.Instance.GetComponent<ScoreManager>();   
    }

    private void OnDisable()
    {
        if(scoreManager && !scoreManager.isGameObver)
        {            
            scoreManager.UpdateUIScore();
        }

    }

}
