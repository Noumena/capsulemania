﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillBoard : MonoBehaviour
{

    [SerializeField] private int m_height = 22;
    public int height { get { return m_height - header; } }

    [SerializeField] private int m_width = 9;
    public int width { get { return m_width; } }
    [SerializeField] private IntUi m_intUi;
    [SerializeField] private int borderSize = 2;
    [SerializeField] private Transform tilePrefabs;
    [SerializeField] private GamePiece[] enemyPrefabs;
    [SerializeField] private int yOffSet, xOffSet;
    

    private int header = 8;
    private int fillHeight, fillWidth;
    private GamePlayManager m_gamePlayManager;

    public GamePiece[,] allGamePieces { get; set; }

    private void Awake()
    {
        allGamePieces = new GamePiece[m_width, m_height];
        SetUpCamera();
        SetUpTile();        
    }

    private void Start()
    {
        if (m_intUi)
        {
            FillBoardWithEnemyAtRandom(m_intUi.lvlUpNumberOfEnemies);
        }     
    }

    private void SetUpCamera()
    {
        Camera.main.transform.position = new Vector3((float)(m_width - 1) / 2f, (float)((m_height - header) - 1) / 2f, -10f);
        float aspectRatio = (float)Screen.width / (float)Screen.height;
        float verticalSize = (float)(m_height - header) / 2 + (float)borderSize;
        float HorizontalSize = ((float)m_width / 2 + (float)borderSize) / aspectRatio;
        Camera.main.orthographicSize = (verticalSize > HorizontalSize) ? verticalSize : HorizontalSize;
    }

    private void SetUpTile()
    {
        for (int x = 0; x < m_width; x++)
        {
            for (int y = 0; y < m_height - header; y++)
            {
                Transform tile = Instantiate(tilePrefabs, new Vector2((float)x, (float)y), Quaternion.identity);
                tile.parent = this.transform;
                tile.name = "[" + x + ", " + y + "]";
            }
        }
    }

    private int FillArea()
    {
        int fillArea;
        fillHeight = (m_height - header) - yOffSet;
        fillWidth = m_width - xOffSet;
        fillArea = fillHeight * fillWidth;
        return fillArea;
    }

    private GamePiece RandomEnemy()
    {
        GamePiece randomEnemy;
        int randomInt = Random.Range(0, enemyPrefabs.Length);

        if (enemyPrefabs[randomInt] != null)
        {
            randomEnemy = enemyPrefabs[randomInt];
            return randomEnemy;
        }
        else
        {
            return null;
        }
    }

    private GamePiece FillEnemyAt(int x, int y)
    {
        GamePiece enemy;
        enemy = Instantiate(RandomEnemy(), Vector3.zero, Quaternion.identity);

        if (enemy)
        {
            PlaceGamePieceAt(enemy, x, y);
            return enemy;
        }
        else
        {
            return null;
        }
    }

    public void FillBoardWithEnemyAtRandom(int numbersOfEnemy)
    {
        int numOfFillEnem = numbersOfEnemy < FillArea() ? numbersOfEnemy : FillArea();
        
            for (int i = 0; i <numOfFillEnem; i++)
            {
                bool IsRepeatedPos = true;
                while (IsRepeatedPos)
                {
                    int randomX = UnityEngine.Random.Range(0, fillWidth);
                    int randomY = UnityEngine.Random.Range(0, fillHeight);

                    if (allGamePieces[randomX, randomY] == null)
                    {
                        FillEnemyAt(randomX, randomY);
                        IsRepeatedPos = false;
                        break;
                    }
                }
            }       
    }

    public bool IsWithinBound(int x, int y)
    {
        return x >= 0 && x < m_width && y >= 0;
    }

    public void PlaceGamePieceAt(GamePiece gamePiece, int x, int y)
    {
        if (gamePiece)
        {
            if (IsWithinBound(x, y))
            {
                gamePiece.transform.position = new Vector3Int(x, y, 0);
                gamePiece.transform.rotation = Quaternion.identity;
                AssignGamePiece(gamePiece, x, y);
                gamePiece.transform.parent = this.transform;
            }
        }
    }

    public void AssignGamePiece(GamePiece gamePiece, int x, int y)
    {
        gamePiece.SetCoordinate(x, y);
        gamePiece.GetFillBoard(this);
        allGamePieces[x, y] = gamePiece;
    }

    public void AssignGamePiece(Shape shape)
    {
        if (shape)
        {
            foreach (GamePiece piece in shape.transform.GetComponentsInChildren<GamePiece>())
            {
                if (piece)
                {
                    var piecePosInt = Vector3Int.RoundToInt(piece.transform.position);
                    AssignGamePiece(piece, piecePosInt.x, piecePosInt.y);
                }
            }
        }
    }

    public bool IsOccupied(Shape shape, int x, int y)
    {
        return allGamePieces[x, y] != null && allGamePieces[x, y].transform.parent != shape.transform;
    }

    public bool IsValidPos(Shape shape)
    {
        foreach (GamePiece piece in shape.transform.GetComponentsInChildren<GamePiece>())
        {
            if (piece)
            {
                var piecePosInt = Vector3Int.RoundToInt(piece.transform.position);

                if (!IsWithinBound(piecePosInt.x, piecePosInt.y))
                {
                    return false;
                }

                if (IsOccupied(shape, piecePosInt.x, piecePosInt.y))
                {
                    return false;
                }
            }
        }

        return true;
    }

    public bool IsOverLimit(Shape shape)
    {
        foreach (GamePiece piece in shape.transform.GetComponentsInChildren<GamePiece>())
        {
            if (piece)
            {
                var piecePosInt = Vector2Int.RoundToInt(piece.transform.position);

                if (IsOverLimit(piecePosInt.y))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public bool IsOverLimit(int y)
    {
        return y >= m_height - header;
    }
}
