﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FindMatch : MonoBehaviour {

    private FillBoard m_fillBoard;

	private void Start ()
    {
        m_fillBoard = GetComponent<FillBoard>();
	}

    private List<GamePiece> FindMatchAt(int startX, int startY, Vector3Int searchDirection, int minLenght)
    {
        List<GamePiece> match = new List<GamePiece>();
        GamePiece startPiece = null;

        if(m_fillBoard.IsWithinBound(startX, startY))
        {
            startPiece = m_fillBoard.allGamePieces[startX, startY];
        }

        if(startPiece)
        {
            match.Add(startPiece);
        }
        else
        {
            return null;
        }

        int width = m_fillBoard.width;
        int height = m_fillBoard.height;
        int nextX;
        int nextY;
        int maxValue = (width > height) ? width : height;
        
        for (int i = 1; i < maxValue -1; i++)
        {
            nextX = startX + (int)Mathf.Clamp((float)searchDirection.x, -1, 1) * i;
            nextY = startY + (int)Mathf.Clamp((float)searchDirection.y, -1, 1) * i;

            if(!m_fillBoard.IsWithinBound(nextX, nextY) || m_fillBoard.IsOverLimit(nextY))
            {
                break;
            }

            GamePiece nextPiece = m_fillBoard.allGamePieces[nextX, nextY];

            if(nextPiece == null)
            {
                break;
            }
            else
            {
                if(nextPiece.matchValue == startPiece.matchValue && !match.Contains(nextPiece))
                {
                    match.Add(nextPiece);
                }
                else
                {
                    break;
                }
            }
        }        
            return match;        
    }

    private List<GamePiece> FindMatchHorizontal(int startX, int startY, int minLenght)
    {
        List<GamePiece> FindForward = FindMatchAt(startX, startY, new Vector3Int(1, 0, 0), minLenght);
        List<GamePiece> FindBackward = FindMatchAt(startX, startY, new Vector3Int(-1, 0, 0), minLenght);

        if(FindForward == null)
        {
            FindForward = new List<GamePiece>();
        }

        if(FindBackward == null)
        {
            FindBackward = new List<GamePiece>();
        }

        var horizontalMatch = FindForward.Union(FindBackward).ToList();
        return (horizontalMatch.Count >= minLenght) ? horizontalMatch : null;
    }

    private List<GamePiece> FindMatchVertical(int startX, int startY, int minLenght)
    {
        List<GamePiece> FindUpward = FindMatchAt(startX, startY, new Vector3Int(0, 1, 0), minLenght);
        List<GamePiece> FindDownward = FindMatchAt(startX, startY, new Vector3Int(0, -1, 0), minLenght);

        if (FindUpward == null)
        {
            FindUpward = new List<GamePiece>();
        }

        if (FindDownward == null)
        {
            FindDownward = new List<GamePiece>();
        }

        var verticalMatch = FindUpward.Union(FindDownward).ToList();
        return (verticalMatch.Count >= minLenght) ? verticalMatch : null;
    }

    private List<GamePiece> FindAllMatchAt(int x, int y, int minLenght)
    {
        List<GamePiece> horizontalMatch = FindMatchHorizontal(x, y, minLenght);
        List<GamePiece> VerticalMatch = FindMatchVertical(x, y, minLenght);

        if(horizontalMatch == null)
        {
            horizontalMatch = new List<GamePiece>();
        }
        
        if(VerticalMatch == null)
        {
            VerticalMatch = new List<GamePiece>();
        }

        var allMatch = horizontalMatch.Union(VerticalMatch).ToList();
        return allMatch;
    }

    public List<GamePiece> FindAllMatchAt(List<GamePiece> gamePieces, int minLenght)
    {
        List<GamePiece> matches = new List<GamePiece>();

        foreach (GamePiece pieces in gamePieces)
        {
            matches = matches.Union(FindAllMatchAt(pieces.xIndex, pieces.yIndex, minLenght)).ToList();
        }
        return matches;
    }

    public List<GamePiece> FindAllMatchAt(Shape shape, int minLenght)
    {
        List<GamePiece> matches = new List<GamePiece>();

        foreach(GamePiece piece in shape.transform.GetComponentsInChildren<GamePiece>())
        {
            if(piece)
            {
                matches = matches.Union(FindAllMatchAt(piece.xIndex, piece.yIndex, minLenght)).ToList();
            }
            else
            {
                return null;
            }
        }       
        return matches;
    }
}
