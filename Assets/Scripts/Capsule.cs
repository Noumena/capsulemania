﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsule : GamePiece {

    private bool isMoving = false;

    public void Move(int x, int y, float timeToMove)
    {
        if (!isMoving)
        {
            StartCoroutine(MoveCoroutine(new Vector3(x, y, 0), timeToMove));
        }
    }

    private IEnumerator MoveCoroutine(Vector3 destinationPos, float timeToMove)
    {
        Vector3 startPos = transform.position;
        bool reachedDistination = false;
        float elapseTime = 0f;
        isMoving = true;

        while (!reachedDistination)
        {
            if (Vector3.Distance(transform.position, destinationPos) < 0.01f)
            {
                reachedDistination = true;

                if (m_fillBoard != null)
                {
                    m_fillBoard.PlaceGamePieceAt(this, (int)destinationPos.x, (int)destinationPos.y);
                    Debug.Log("Reach destination");
                }

                break;
            }

            elapseTime += Time.deltaTime;
            float t = elapseTime / timeToMove;
            transform.position = Vector3.Lerp(startPos, destinationPos, t);
            yield return null;
        }

        isMoving = false;
    }
}
