﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCapsule : MonoBehaviour{

    [SerializeField] Shape[] shapePrefab;
   	
	private Shape GetRandomShape()
    {        
        int randomIndex = Random.Range(0, shapePrefab.Length);
        return shapePrefab[randomIndex];
    }

    public Shape SpawnShape()
    {
        Shape shape = GetRandomShape();
        if(shape)
        {
            FillBoard fillBoard = GetComponent<FillBoard>();
            int width = fillBoard.width;
            int height = fillBoard.height;
            int spawnPosX = (int)width / 2;
            int spawnPosy = (int)height + 1;
            Vector3Int spawnPos = new Vector3Int(spawnPosX, spawnPosy, 0);
            Shape spawnShape = Instantiate(shape, spawnPos, Quaternion.identity);
            return spawnShape;
        }

        else
        {
            return null;
        }
    }
}
