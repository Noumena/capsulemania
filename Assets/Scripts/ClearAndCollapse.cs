﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ClearAndCollapse : MonoBehaviour {

    [SerializeField] private float m_swapTime;
    private FillBoard m_fillBoard;
    private SoundManager m_soundManager;
    private bool IsPieceClear = false;

	private void Start ()
    {
        m_fillBoard = GetComponent<FillBoard>();
        m_soundManager = GamePlayManager.Instance.GetComponent<SoundManager>();
	}

    private void ClearPieceAt(List<GamePiece> gamePieces)
    {
        gamePieces.OrderBy(x => x.yIndex).ToList();

        foreach (GamePiece piece in gamePieces)
        {
            ClearPieceAt(piece.xIndex, piece.yIndex);
        }

        if (IsPieceClear)
        {
            if (m_soundManager)
            {
                PlaySound(m_soundManager.clearSound);
            }

            IsPieceClear = false;
        }        
    }

    private void ClearPieceAt(int x, int y)
    {
        GamePiece clearPiece = m_fillBoard.allGamePieces[x, y];

        if (clearPiece != null)
        {
            m_fillBoard.allGamePieces[x, y] = null;
            DestroyImmediate(clearPiece.gameObject);
            IsPieceClear = true;
        }
    }

    private List<GamePiece> CollapeColumn(int column, int row, float swapTime)
    {
        List<GamePiece> movingPiece = new List<GamePiece>();
        Debug.Log("row pos: " + GetRowPosToMove(column, row));

       for (int i = GetRowPosToMove(column, row); i < row; i++)
       {           
          if(m_fillBoard.allGamePieces[column, i] == null)
            {
                for(int j = row; j > i; j--)
                {
                    if (m_fillBoard.allGamePieces[column, j] != null && m_fillBoard.allGamePieces[column, j].GetComponent<Capsule>())
                    {                       
                        m_fillBoard.allGamePieces[column, j].GetComponent<Capsule>().Move(column, i, swapTime);
                        m_fillBoard.allGamePieces[column, i] = m_fillBoard.allGamePieces[column, j];
                        m_fillBoard.allGamePieces[column, i].SetCoordinate(column, i);

                        if (!movingPiece.Contains(m_fillBoard.allGamePieces[column, i]))
                        {
                            movingPiece.Add(m_fillBoard.allGamePieces[column, i]);
                        }

                        m_fillBoard.allGamePieces[column, j] = null;
                        break;                                                
                    }
                }
            }
       }
        return movingPiece;
    }//change column + 1 and column -1

    private int GetRowPosToMove(int column, int row)
    {
        int rowPos = 0;
        for(int i = 0; i < row; i++ )
        {
            if(m_fillBoard.allGamePieces[column, i] != null)
            {
                if(m_fillBoard.allGamePieces[column, i].yIndex >= rowPos)
                {
                    rowPos = m_fillBoard.allGamePieces[column, i].yIndex;
                }                
            }
        }
        return rowPos;
    }

    public List<GamePiece> ClearAndCollpse(List<GamePiece> gamePieces)
    {
        List<Vector3Int> findMatchPoses = GetCollapsePos(gamePieces);
        List<GamePiece> getPieceTomove = new List<GamePiece>();
        List<GamePiece> movingPieces = new List<GamePiece>();
        List<Vector3Int> movingPoses = new List<Vector3Int>();

        ClearPieceAt(gamePieces);

        foreach (Vector3Int findMatchPos in findMatchPoses)
        {
            getPieceTomove = getPieceTomove.Union(GetPiecesToMove(findMatchPos.x)).ToList();
        }

        List<Vector3Int> getPieceToMovePoses = GetCollapsePos(getPieceTomove);

        foreach (Vector3Int findMatchPos in findMatchPoses)
        {
            foreach (Vector3Int getPieceToMovePos in getPieceToMovePoses)
            {
                if (findMatchPos.x == getPieceToMovePos.x)
                {
                    movingPoses.Add(new Vector3Int(findMatchPos.x, getPieceToMovePos.y, 0));
                }
            }
        }

        movingPieces = CollapsePieces(movingPieces, movingPoses);

        findMatchPoses = findMatchPoses.OrderBy(x => x.y).ToList();
        movingPieces = CollapseNextPiece(findMatchPoses, movingPieces);

        return movingPieces;
    }

    private List<GamePiece> CollapsePieces(List<GamePiece> movingPieces, List<Vector3Int> movingPoses)
    {
        foreach (Vector3Int movingPos in movingPoses)
        {
            movingPieces = movingPieces.Union(CollapeColumn(movingPos.x, movingPos.y, m_swapTime)).ToList();
        }
       
        return movingPieces;
    }

    private List<GamePiece> CollapseNextPiece(List<Vector3Int> Poses, List<GamePiece> gamePieces)
    {
        foreach (Vector3Int pos in Poses)
        {
            if (CheckNextGamePiece(pos.x, pos.y) != -1)
            {
                List<GamePiece> getNextPieceToMove = GetPiecesToMove(CheckNextGamePiece(pos.x, pos.y));
                List<Vector3Int> getCollapseNextPoses = GetCollapsePos(getNextPieceToMove);
                getCollapseNextPoses = getCollapseNextPoses.OrderBy(x => x.y).ToList();

                foreach (Vector3Int getCollapseNextPos in getCollapseNextPoses)
                {
                    gamePieces = gamePieces.Union(CollapeColumn(getCollapseNextPos.x, getCollapseNextPos.y, m_swapTime)).ToList();
                }
            }
        }
      
        return gamePieces;
    }

    private List<Vector3Int> GetCollapsePos(List<GamePiece> gamePiece)
    {
        List<Vector3Int> collapsePos  = new List<Vector3Int>();
        foreach (GamePiece piece in gamePiece)
        {
            if(piece)
            {
                Vector3Int piecePosInt = Vector3Int.RoundToInt(piece.transform.position);
                collapsePos.Add(piecePosInt);
            }                        
        }

        return collapsePos;
    }

    private List<GamePiece> GetPiecesToMove(int column)
    {
        List<GamePiece> getPieces = new List<GamePiece>();

        for (int i = 0; i < m_fillBoard.height; i++)
        {
            if (m_fillBoard.allGamePieces[column, i] != null && !getPieces.Contains(m_fillBoard.allGamePieces[column, i]))
            {
                getPieces.Add(m_fillBoard.allGamePieces[column, i]);
            }
        }

        return getPieces;
    }

    private int CheckNextGamePiece(int x, int y)
    {
        if(m_fillBoard.IsWithinBound(x + 1, y))
        {
            if (m_fillBoard.allGamePieces[x + 1, y])
            {
                if(m_fillBoard.allGamePieces[x + 1, y].transform.parent.GetComponent<Shape>())
                {
                    Shape shape = m_fillBoard.allGamePieces[x + 1, y].transform.parent.GetComponent<Shape>();

                    if(shape.transform.childCount < shape.numberOfCapsules)
                    {
                        return  x + 1;
                    }
                    else
                    {
                        return -1;
                    }
                }                
            }
        }

        if(m_fillBoard.IsWithinBound(x -1, y))
        {
            if (m_fillBoard.allGamePieces[x - 1, y])
            {
                if (m_fillBoard.allGamePieces[x - 1, y].transform.parent.GetComponent<Shape>())
                {
                    Shape shape = m_fillBoard.allGamePieces[x - 1, y].transform.parent.GetComponent<Shape>();

                    if (shape.transform.childCount < shape.numberOfCapsules)
                    {
                        return  x - 1;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }

        return -1;
    }

    public bool IsCollapse (List<GamePiece> gamePieces)
    {
        foreach(GamePiece piece in gamePieces)
        {
            if(piece)
            {
                if(piece.transform.position.y - (float)piece.yIndex > 0.01f)
                {
                    return false;
                }
            }
        }
        return true;
    }

    private void PlaySound(AudioClip clip)
    {
        if(clip)
        {
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, m_soundManager.fxVolume);
        }
    }

}
