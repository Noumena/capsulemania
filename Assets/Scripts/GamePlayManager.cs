﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayManager : MonoBehaviour {

    private static GamePlayManager instance;
    public static GamePlayManager Instance { get; private set; }
    [SerializeField] private float m_dropInterval = 0.1f, m_keyRepeatRateLeftRight = 0.25f, m_keyRepeatRateDown = 0.01f, m_keyRepeatRateRotate = 0.25f;
    [SerializeField] private int m_findMatchMinLenght = 4;
    private FillBoard m_fillBoard;
    private SpawnCapsule m_spawnCapsule;
    private FindMatch m_findMatch;
    private Shape m_activeShape;
    private ClearAndCollapse m_clearAndCollapse;
    private ScoreManager m_scoreManager;
    private SoundManager m_soundManager;
    private float m_timeToNextKeyDown, m_timeToNextKeyLeftRight, m_timeToNextKeyRotate, m_timeToDrop;
    

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start ()
    {
        SetUpForGamePlay();

        if (m_activeShape == null)
        {
            m_activeShape = m_spawnCapsule.SpawnShape();
        }

        SetKeyInput();
    }

    private void SetUpForGamePlay()
    {
        m_spawnCapsule = GetComponentInChildren<SpawnCapsule>();
        m_fillBoard = GetComponentInChildren<FillBoard>();
        m_findMatch = GetComponentInChildren<FindMatch>();
        m_clearAndCollapse = GetComponentInChildren<ClearAndCollapse>();
        m_scoreManager = GetComponent<ScoreManager>();
        m_soundManager = GetComponent<SoundManager>();
    }

    private void SetKeyInput()
    {
        m_timeToDrop = Time.time + m_dropInterval;
        m_timeToNextKeyDown = Time.time + m_keyRepeatRateDown;
        m_timeToNextKeyRotate = Time.time + m_timeToNextKeyRotate;
        m_timeToNextKeyLeftRight = Time.time + m_keyRepeatRateLeftRight;
    }

    private void Update ()
    {
		if(m_activeShape && m_spawnCapsule && m_fillBoard && m_findMatch && m_scoreManager && m_soundManager)
        {
            PlayerInput();
        }
    }

    private void PlayerInput()
    {
        if (Time.time > m_timeToDrop || (Input.GetKey("s") && Time.time > m_timeToNextKeyDown))
        {
            m_activeShape.MoveDown();
            m_timeToDrop = Time.time + m_dropInterval;
            m_timeToNextKeyDown = Time.time + m_keyRepeatRateDown;

            if (!m_fillBoard.IsValidPos(m_activeShape))
            {
                if(m_fillBoard.IsOverLimit(m_activeShape))
                {
                    m_scoreManager.ShowGameOverPanel();
                }
                else
                {
                    LandShape();
                }                
            }
        }

        else if ((Input.GetKey("a")) && Time.time > m_timeToNextKeyLeftRight)
        {
            m_activeShape.MoveLeft();
            m_timeToNextKeyLeftRight = Time.time + m_keyRepeatRateLeftRight;

            if (!m_fillBoard.IsValidPos(m_activeShape))
            {
                m_activeShape.MoveRight();
                PlaySound(m_soundManager.errorSound);
            }
            else
            {
                PlaySound(m_soundManager.moveSound);
            }
        }

        else if ((Input.GetKey("d")) && Time.time > m_timeToNextKeyLeftRight)
        {
            m_activeShape.MoveRight();
            m_timeToNextKeyLeftRight = Time.time + m_keyRepeatRateLeftRight;

            if (!m_fillBoard.IsValidPos(m_activeShape))
            {
                m_activeShape.MoveLeft();
                PlaySound(m_soundManager.errorSound);
            }
            else
            {
                PlaySound(m_soundManager.moveSound);
            }
        }

        else if ((Input.GetKey("w")) && Time.time > m_timeToNextKeyRotate)
        {
            m_activeShape.RotateLeft();
            m_timeToNextKeyRotate = Time.time + m_keyRepeatRateRotate;

            if (!m_fillBoard.IsValidPos(m_activeShape))
            {
                m_activeShape.RotateRight();
                PlaySound(m_soundManager.errorSound);
            }
            else
            {
                PlaySound(m_soundManager.moveSound);
            }

        }
    }

    private void LandShape()
    {
        m_activeShape.MoveUp();
        PlaySound(m_soundManager.dropSound);
        m_fillBoard.AssignGamePiece(m_activeShape);
        StartCoroutine(ClearAndCollapseRoutine(m_findMatch.FindAllMatchAt(m_activeShape, m_findMatchMinLenght)));
        m_activeShape = null;
        m_activeShape = m_spawnCapsule.SpawnShape();
        m_timeToNextKeyDown = Time.time + m_keyRepeatRateDown;
    }

    private IEnumerator ClearAndCollapseRoutine(List<GamePiece> gamePieces)
    {
        List<GamePiece> matches = new List<GamePiece>();
        List<GamePiece> movingPieces = new List<GamePiece>();

        bool isFinished = false;

        while(!isFinished)
        {            
          movingPieces = m_clearAndCollapse.ClearAndCollpse(gamePieces);

            while(!m_clearAndCollapse.IsCollapse(movingPieces))
            {
                yield return null;
            }

            matches = m_findMatch.FindAllMatchAt(movingPieces, m_findMatchMinLenght);

            if(matches.Count == 0)
            {
                isFinished = true;
                break;
            }
            else
            {
                yield return StartCoroutine(ClearAndCollapseRoutine(matches));
            }
        }

        yield return null;
    }

    private void PlaySound(AudioClip clip)
    {
        if (clip)
        {
            AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, m_soundManager.fxVolume);
        }
    }
}
