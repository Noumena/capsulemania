﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour {

    public int numberOfCapsules
    {
        private set;  get; 
    }

    private void Awake()
    {
        GetNumberOfCapsules();
    }

    private void GetNumberOfCapsules()
    {
        int numberOfChilds = transform.childCount;

        for (int i = 0; i < numberOfChilds; i++)
        {
            if (transform.GetChild(i).GetComponent<Capsule>())
            {
                numberOfCapsules++;
            }
        }
    }

    private void Move(Vector3Int moveDirection)
    {
        transform.position += moveDirection;       
    }

    public void MoveLeft()
    {
        Move(new Vector3Int(-1, 0, 0));
    }

    public void MoveRight()
    {
        Move(new Vector3Int(1, 0, 0));        
    }

    public void MoveUp()
    {
        Move(new Vector3Int(0, 1, 0));
    }

    public void MoveDown()
    {
        Move(new Vector3Int(0, -1, 0));
    }

    public void RotateRight()
    {
        transform.Rotate(0, 0, -90);
    }

    public void RotateLeft()
    {
        transform.Rotate(0, 0, 90);
    }
}
