﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour {
   
    [SerializeField] private int numberOfEnemPerLv, initialScore = 0, initialNOfEnem, initailLevel = 1, pointsPerEnem, padZeroInt;

    [SerializeField] private IntUi m_intUi;

    [SerializeField] private Text scoreText;
    [SerializeField] private Text gameOverScoreText, pauseScoreText, winScoreText;
    [SerializeField] private Text levelText;
    [SerializeField] private Text enemiesText;
    [SerializeField] private Text gameOverHighScoreText, pauseHighScoreText, winHighScoreText;

    [SerializeField] private CanvasRenderer m_gameOverpanel, m_pausePanel, m_winPanel;

    private SoundManager m_soundManager;
    private bool m_isPaused = false; 

    public bool isGameObver { get; set; }

    private void Awake()
    {
        if (m_intUi)
        {
            if (!m_intUi.OnNextLevel)
            {
                Reset();
            }
            else
            {
                UpdateUIText();
            }
        }

        Time.timeScale = 1f;
        m_soundManager = GetComponent<SoundManager>();
        isGameObver = false;

    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (m_isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Pause()
    {
        if (!isGameObver)
        {
            if (m_pausePanel)
            {
                UpdatePauseUIText();
                m_pausePanel.gameObject.SetActive(true);
                Time.timeScale = 0f;
                m_isPaused = true;
            }
        }        
    }

    public void Resume()
    {
        if (m_pausePanel)
        {
            m_pausePanel.gameObject.SetActive(false);
            Time.timeScale = 1f;
            m_isPaused = false;
        }
    }

    private void Reset()
    {
        if (m_intUi)
        {
            m_intUi.score = initialScore;
            m_intUi.level = initailLevel;
            m_intUi.lvlUpNumberOfEnemies = initialNOfEnem;
            m_intUi.numberOfEnemies = initialNOfEnem;
            UpdateUIText();
        }
    }

    public void UpdateUIScore()
    {
        if(m_intUi)
        {
            m_intUi.score += pointsPerEnem;
            m_intUi.numberOfEnemies -= 1;

            if (m_intUi.score > m_intUi.highScore)
            {
                m_intUi.highScore = m_intUi.score;
            }

            if (m_intUi.numberOfEnemies <= 0)
            {
                m_intUi.lvlUpNumberOfEnemies += numberOfEnemPerLv;
                m_intUi.numberOfEnemies = m_intUi.lvlUpNumberOfEnemies;
                WinText();
                m_intUi.level++;
                ShowWinPanel();
            }

            UpdateUIText();
        }           
    }

    private void WinText()
    {
        if (m_winPanel)
        {
            Text winnerText = m_winPanel.transform.GetChild(0).GetComponent<Text>();

            if (winnerText)
            {
                winnerText.text = m_intUi.level.ToString() + " Level Complete";
            }
        }
    }

    private void UpdateUIText()
    {
        if (scoreText)
        {
            scoreText.text = PadZero(m_intUi.score, padZeroInt);
        }
        
        if (levelText)
        {
            levelText.text = m_intUi.level.ToString();
        }

        if (enemiesText)
        {
            enemiesText.text = m_intUi.numberOfEnemies.ToString();
        }       
    }

    private void UpdateGameOverUIText()
    {
        if (gameOverScoreText)
        {
            gameOverScoreText.text = PadZero(m_intUi.score, padZeroInt);
        }

        if (gameOverHighScoreText)
        {
            gameOverHighScoreText.text = PadZero(m_intUi.highScore, padZeroInt);
        }
    }

    private void UpdatePauseUIText()
    {
        if (pauseScoreText)
        {
            pauseScoreText.text = PadZero(m_intUi.score, padZeroInt);
        }

        if (pauseHighScoreText)
        {
            pauseHighScoreText.text = PadZero(m_intUi.highScore, padZeroInt);
        }
    }

    private void UpdateWinUIText()
    {
        if (winScoreText)
        {
            winScoreText.text = PadZero(m_intUi.score, padZeroInt);
        }

        if (winHighScoreText)
        {
            winHighScoreText.text = PadZero(m_intUi.highScore, padZeroInt);
        }
    }

    private string PadZero(int numbers, int padDigits)
    {
        string str = numbers.ToString();
        while(str.Length < padDigits)
        {
            str = "0" + str;
        }
        return str;
    }

    public void RestartLevel()
    {
        if (m_intUi)
        {
            m_intUi.OnNextLevel = false;
        }
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ContinueToNextLevel()
    {
        if (m_intUi)
        {
            m_intUi.OnNextLevel = true;
        }
        
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ShowGameOverPanel()
    {
        if (m_soundManager)
        {
            m_soundManager.PlayBackGroundMusic(m_soundManager.gameOverSound);
        }

        if(m_gameOverpanel)
        {
            UpdateGameOverUIText();
            m_gameOverpanel.gameObject.SetActive(true);
            Time.timeScale = 0f;
            isGameObver = true;
        }
    }

    private void ShowWinPanel()
    {
        if (m_soundManager)
        {
            m_soundManager.PlayBackGroundMusic(m_soundManager.winSound);
        }

        if (m_winPanel)
        {
            UpdateWinUIText();
            m_winPanel.gameObject.SetActive(true);
            Time.timeScale = 0f;
            isGameObver = true;
        }
    }

}
