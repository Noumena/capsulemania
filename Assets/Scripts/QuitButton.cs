﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour {

	// Use this for initialization
	private void Start ()
    {
        GetComponent<Button>().onClick.AddListener(OnClickQuitButton);
		
	}

    private void OnClickQuitButton()
    {
        Debug.Log("Quit the game");
        Application.Quit();        
    }
}
