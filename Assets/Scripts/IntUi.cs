﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class IntUi : ScriptableObject {
    
    public int lvlUpNumberOfEnemies { get; set; }
    public int numberOfEnemies { set; get; }
    public int score { set; get; }
    public int highScore { set; get; }
    public int level { set; get; }
    public bool OnNextLevel { set; get; }    
}
