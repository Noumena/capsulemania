﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePiece : MonoBehaviour {


    [SerializeField] private bool m_isCapsule, m_isEnemy;

    public bool isCapsule { get { return m_isCapsule; } }

    public bool isEnemy { get { return m_isEnemy; } }

    public enum MatchValue
    {
        Red,
        Grey,
        Green
    }

    [SerializeField] private MatchValue m_matchValue;

    public MatchValue matchValue
    {
        get { return m_matchValue; }  
    }

    public int xIndex
    {
        get; private set;
    }
    
    public int yIndex
    {
        get; private set;        
    }

    protected FillBoard m_fillBoard;


    void Start () {
		
	}

    public void SetCoordinate(int x, int y)
    {
        xIndex = x;
        yIndex = y;
    }

    public void GetFillBoard(FillBoard fillBoard)
    {
        m_fillBoard = fillBoard;
    }


    void Update () {
		
	}
}
